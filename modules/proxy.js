const proxyApis = {
	'/fs-forms/': {
		target: process.env.WP_JSON_URL,
		auth: `${process.env.G_FORMS_K}:${process.env.G_FORMS_K_SECRET}`,
		pathRewrite: { '^/fs-forms/': '/gf/v2/forms/' },
		changeOrigin: true,
	},
	'/api/': { target: process.env.WP_JSON_URL, pathRewrite: { '^/api/': '/' } },
	'/content/': {
		target: process.env.WP_JSON_URL,
		pathRewrite: { '^/content/': '/wp/v2/' }
	},
};
module.exports = proxyApis;

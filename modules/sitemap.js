// eslint-disable-next-line import/no-extraneous-dependencies
import { all, get, spread } from 'axios';

const sitemap = {
	path: '/sitemap.xml',
	hostname: process.env.HOME_URL,
	generate: false,
	routes() {
		return all([
			get(`${process.env.WP_JSON_URL}rmh/v1/post_links`),
		])
			.then(spread((posts) => [...posts.data]))
			.then((posts) => posts.map((item) => ({
				url: item.slug,
				changefreq: 'daily',
				priority: 1,
				lastmodISO: new Date().toISOString()
			})));
	}
};
module.exports = sitemap;

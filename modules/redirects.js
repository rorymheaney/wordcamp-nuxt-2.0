const redirects = [
	{
		from: '^/posts/(.*)$',
		to: (from, req) => `/${req}`,
		statusCode: 301
	},
	{
		from: '^/admin',
		to: process.env.ADMIN_URL
	}
];
module.exports = redirects;

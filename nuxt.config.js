// modules
const SITE_MAP = require('./modules/sitemap.js');
const SITE_REDIRECTS = require('./modules/redirects.js');
const SITE_PROXY = require('./modules/proxy');

export default {
	/*
	 ** Nuxt rendering mode
	 ** See https://nuxtjs.org/api/configuration-mode
	 */
	mode: 'universal',
	/*
	 ** Nuxt target
	 ** See https://nuxtjs.org/api/configuration-target
	 */
	target: 'server',
	/*
	 ** Headers of the page
	 ** See https://nuxtjs.org/api/configuration-head
	 */
	head: {
		htmlAttrs: {
			lang: 'en-US',
		},
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || '',
			},
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: {
		color: '#212121',
		height: '8px'
	},
	/*
	 ** Global CSS
	 */
	css: [
		'~/assets/scss/main.scss'
	],
	/*
	 ** Plugins to load before mounting the App
	 ** https://nuxtjs.org/guide/plugins
	 */
	plugins: [
		{ src: '~plugins/vee-validate.js' },
		{ src: '~plugins/v-mask.js', ssr: false }
	],
	/*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
	components: true,
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://bootstrap-vue.js.org
		'bootstrap-vue/nuxt',
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		'@nuxtjs/sitemap',
		'@nuxtjs/redirect-module',
		'nuxt-trailingslash-module',
		'@nuxtjs/robots',
		['nuxt-env', {
			keys: [
				{ key: 'JSON_URL', default: process.env.WP_JSON_URL },
				{ key: 'SITE_HOME_URL', default: process.env.HOME_URL }
			]
		}],
	],
	bootstrapVue: {
		bootstrapCSS: false, // Or `css: false`
		bootstrapVueCSS: false, // Or `bvCSS: false`
	},
	robots: {
		Sitemap: `${process.env.HOME_URL}sitemap.xml`
	},
	sitemap: SITE_MAP,
	redirect: SITE_REDIRECTS,
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {
		credentials: true,
		proxy: true
	},
	/*
	 ** updated router for posts
	 ** posts/your-post - /your-posts
	 */
	// router: {
	// 	extendRoutes(routes, resolve) {
	// 		routes.push({
	// 			path: '/:index',
	// 			component: resolve(__dirname, 'pages/posts/_slug/index.vue')
	// 		});
	// 	}
	// },
	/*
	 ** set up proxy url
	 ** pass creds for gravity forms etc
	 */
	proxy: SITE_PROXY,
	/*
	 ** Build configuration
	 ** See https://nuxtjs.org/api/configuration-build/
	 */
	build: {
		transpile: ['vee-validate/dist/rules'],
	},
};

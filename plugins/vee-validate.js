import { extend } from 'vee-validate';
// eslint-disable-next-line no-unused-vars
import { required, alpha } from 'vee-validate/dist/rules';

extend('alpha', {
	...alpha,
	message: 'This field must only contain alphabetic characters'
});
